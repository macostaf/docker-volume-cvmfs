FROM fedora:26
MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

LABEL name="docker-volume-cvmfs" \
      maintainer="Ricardo Rocha <ricardo.rocha@cern.ch>" \
      license="Apache2" \
      summary="CVMFS volume container" \
      version="0.1" \
      help="None" \
      architecture="x86_64" \
      atomic.type="system" \
      distribution-scope="public"

RUN dnf -y update && dnf clean all

RUN dnf install -y \
	wget

RUN wget -q https://ecsft.cern.ch/dist/cvmfs/cvmfs-2.1.20/cvmfs-2.1.20-1.fc21.x86_64.rpm; \
	wget -q https://ecsft.cern.ch/dist/cvmfs/cvmfs-config/cvmfs-config-default-latest.noarch.rpm; \
	yum -y install cvmfs-2.1.20-1.fc21.x86_64.rpm cvmfs-config-default-latest.noarch.rpm

COPY service.template config.json.template manifest.json tmpfiles.template /exports/

RUN rmdir /cvmfs

RUN echo $'\n\
CVMFS_HTTP_PROXY="http://ca-proxy.cern.ch:3128" \n\
CVMFS_CACHE_BASE=/var/cache/cvmfs \n\
CVMFS_QUOTA_LIMIT=20000 \n'\
>> /etc/cvmfs/default.local

ADD docker-volume-cvmfs /usr/sbin/docker-volume-cvmfs
RUN chmod 755 /usr/sbin/docker-volume-cvmfs

ADD dockercvmfs.pp /dockercvmfs.pp

RUN mkdir -p /var/lib/kubelet/plugins/volume/exec/cern~cvmfs

ADD doit.sh /doit.sh

CMD ["/doit.sh"]
